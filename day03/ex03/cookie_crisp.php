<?php
    $name = $_GET['name'];
    if ($name != NULL && $name != '') {
        switch ($_GET['action']) {
            case 'set':
                setcookie($name, $_GET['value'], time() + 3600, "/");
                break;
            case 'get':
                if ($_COOKIE[$name] != NULL)
                    echo $_COOKIE[$name]."\n";
                break;
            case 'del':
                setcookie($name, '', time() - 3600, "/");
                break;
        }
    }
?>
