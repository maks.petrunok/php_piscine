#!/usr/bin/php
<?php

while (!feof(STDIN))
{
	echo "Enter a number: ";
	$str = trim(fgets(STDIN));

	if (is_numeric($str))
	{
		if ($str % 2 == 0)
			echo "The number $str is even";
		else
			echo "The number $str is odd";
	}
	else if (!feof(STDIN))
	{
		echo "'$str' is not a number";
	}
	echo "\n";
}

?>
