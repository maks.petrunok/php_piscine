<?php

function ft_is_sort($tab)
{
	$asc = $desc = true;
	$size = count($tab);
	for ($i = 1; $i < $size; $i++)
	{
		if ($tab[$i] > $tab[$i - 1])
			$desc = false;
		if ($tab[$i] < $tab[$i - 1])
			$asc = false;
		if (!$asc && !$desc)
			break;
	}
	return ($asc || $desc);
}

?>
