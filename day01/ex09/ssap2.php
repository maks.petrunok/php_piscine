#!/usr/bin/php
<?php

function score($ch)
{
	$score = ord($ch);
	if ($score >= ord('A') && $score <= ord('Z'))
		$score *= 1;
	else if ($score >= ord('0') && $score <= ord('9'))
		$score *= 10;
	else
		$score *= 100;
	return ($score);
}

function cmp($a, $b)
{
	$len_a = strlen($a);
	$len_b = strlen($b);
	for ($i = 0; $i < $len_a && $i < $len_b; $i++)
	{
		$aa = score(strtoupper(substr($a, $i, 1)));
		$bb = score(strtoupper(substr($b, $i, 1)));
		if ($aa != $bb)
			return ($aa - $bb);
	}
	return (0);
}

function ft_split($str)
{
	$str = trim($str);
	$str = preg_replace("#\s{2,}#", " ", $str);
	$arr = explode(' ', $str);
	return ($arr);
}

$arr = array();

for ($i = 1; $i < $argc; $i++)
{
	$tmp = ft_split($argv[$i]);
	$arr = array_merge($arr, $tmp);

}

usort($arr, "cmp");

foreach ($arr as $val)
	echo "$val\n";

?>
