#!/usr/bin/php
<?php

function remove_spaces($str)
{
	$str = trim($str);
	$str = preg_replace("#\s{2,}#", " ", $str);
	return ($str);
}

if ($argc == 2)
{
	$str = remove_spaces($argv[1]);
	echo "$str\n";
}

?>
