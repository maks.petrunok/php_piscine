#!/usr/bin/php
<?php

function ft_split($str)
{
	$str = trim($str);
	$str = preg_replace("#\s{2,}#", " ", $str);
	$arr = explode(' ', $str);
	return ($arr);
}

$arr = array();

for ($i = 1; $i < $argc; $i++)
{
	$tmp = ft_split($argv[$i]);
	$arr = array_merge($arr, $tmp);

}

sort($arr);

foreach ($arr as $val)
	echo "$val\n";

?>
