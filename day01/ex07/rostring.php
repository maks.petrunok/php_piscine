#!/usr/bin/php
<?php

function ft_split($str)
{
	$str = trim($str);
	$str = preg_replace("#\s{2,}#", " ", $str);
	$arr = explode(' ', $str);
	return ($arr);
}

if ($argc > 1)
{
	$arr = ft_split($argv[1]);
	$arr[count($arr)] = $arr[0];
	unset($arr[0]);
	$str = "";
	foreach ($arr as $value)
		$str = $str." ".$value;
	$str = trim($str);
	echo "$str\n";
}

?>
