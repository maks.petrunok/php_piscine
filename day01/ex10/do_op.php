#!/usr/bin/php
<?php

function process($a, $op, $b)
{
	switch ($op) {
		case '+':
			$res = $a + $b;
			break;
		case '-':
			$res = $a - $b;
			break;
		case '*':
			$res = $a * $b;
			break;
		case '/':
			$res = $a / $b;
			break;
		case '%':
			$res = $a % $b;
			break;
	}
	return ($res);
}

if ($argc == 4)
{
	$a = trim($argv[1]);
	$op = trim($argv[2]);
	$b = trim($argv[3]);

	if ($b == 0 && preg_match("#^[\/\%]$#", $op))
		echo "Division by zero\n";
	else if (is_numeric($a) && is_numeric($b)
			 && preg_match("#^[\+\-\*/%]$#", $op))
		echo process($a, $op, $b)."\n";
	else
		echo "There are errors in input.\n";
}
else
	echo "Incorrect Parameters\n";

?>
