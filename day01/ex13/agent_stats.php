#!/usr/bin/php
<?php

if ($argc != 2)
	exit();

$rows = array();
$users = array();

$str = fgets(STDIN);
while (!feof(STDIN))
{
	$str = trim(fgets(STDIN));
	$vals = explode(";", $str);
	if (count($vals) == 4)
	{
		$rows[] = $vals;
	}
}

if ($argv[1] === "average")
{
	$count = 0;
	$grade = 0;
	foreach ($rows as $val) {
		if ($val[1] != '' && $val[2] !== "moulinette") {
			$count++;
			$grade += $val[1];
		}
	}
	echo ($grade / $count)."\n";
}
else if ($argv[1] === "average_user" || $argv[1] === "moulinette_variance")
{
	foreach ($rows as $val) {
		if (!array_key_exists($val[0], $users))
			$users[$val[0]] = array(0, 0, 0);
		if ($val[1] !== '' && $val[2] !== "moulinette") {
			$users[$val[0]][0]++;
			$users[$val[0]][1] += $val[1];
		} else if ($val[2] === "moulinette")
			$users[$val[0]][2] += $val[1];
	}
	ksort($users);
	if ($argv[1] === "average_user")
		foreach($users as $key => $value)
			echo "$key:".($value[1] / $value[0])."\n";
	else
		foreach($users as $key => $value)
			echo "$key:".(($value[1]) / $value[0] - $value[2])."\n";
}

?>
