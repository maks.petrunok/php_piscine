#!/usr/bin/php
<?php

function process($a, $op, $b)
{
	switch ($op) {
		case '+':
			$res = $a + $b;
			break;
		case '-':
			$res = $a - $b;
			break;
		case '*':
			$res = $a * $b;
			break;
		case '/':
			$res = $a / $b;
			break;
		case '%':
			$res = $a % $b;
			break;
	}
	return ($res);
}

function set_args(&$a, &$b, $expr)
{
	$args = preg_split("#[\+\-\*/%]#", $expr);
	if (count($args) == 2)
	{
		$a = trim($args[0]);
		$b = trim($args[1]);
		if (is_numeric($a) && is_numeric($b))
			return (true);
	}
	else
		return (false);
}

function set_op(&$op, $expr, $position)
{
	$op = substr($expr, $position, 1);
	if (preg_match("#[\+\-\*/%]#", $op))
		return (true);
	else
		return (false);
}

if ($argc == 2)
{
	$argv[1] = str_replace(" ", "", $argv[1]);
	if (set_args($a, $b, $argv[1]) && set_op($op, $argv[1], strlen($a)))
	{
		if ($b == 0 && preg_match("#^[\/\%]$#", $op))
			echo "Division by zero\n";
		else
			 echo process($a, $op, $b)."\n";
	}
	else
		echo "Syntax Error\n";
}
else
	echo "Incorrect Parameters\n";

?>
