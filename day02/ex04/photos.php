#!/usr/bin/php
<?php

function parse_img($html, $domen) {
    preg_match_all("|<\s*img.*src=\".*>|siU", $html, $matches, PREG_PATTERN_ORDER);
    $images = array();
    foreach ($matches[0] as $img) {
        preg_match("|src=\"[^\"]*\"|si", $img, $m);
        $tmp = substr($m[0], 5, strlen($m[0]) - 6);
        if (!strpos($tmp, $domen) && !preg_match("/^https?:\/\/.*/", $tmp))
            $tmp = $domen."/".$tmp;
        $images[] = $tmp;
    }
    return ($images);
}

function read_page($url) {
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $page = curl_exec($curl);
    curl_close($curl);
    return $page;
}

function create_folder($name) {
    $name = preg_replace("|^https?:\/\/|", "", $name);
    $name = preg_replace("|\/*$|", "", $name);
    $name = str_replace("/", ".", $name);
    if (!is_dir($name))
        mkdir($name);
    return ($name);
}

function download($imgs, $dir) {
    foreach($imgs as $img) {
        $file = fopen($dir."/".basename($img), 'w');
        $curl = curl_init($img);
        curl_setopt($curl, CURLOPT_FILE, $file);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_exec($curl);
        curl_close($curl);
        fflush($file);
        fclose($file);
    }
}

if ($argc != 2)
    exit();
$html = read_page($argv[1]);
$imgs = parse_img($html, $argv[1]);
if (count($imgs) > 0) {
    $dir = create_folder($argv[1]);
    download($imgs, $dir);
}

?>