#!/usr/bin/php
<?php

if ($argc != 2) {
    if ($argc > 2)
        echo "Too many arguments\n";
    exit();
}
if (!($file = file($argv[1]))) {
    echo "Error while reading file\n";
    exit();
}

$content = "";
foreach($file as $str) {
    $content .= $str;
}

$content = preg_replace_callback("/<a.* title=\".*\".*<\/a>/isU", replace_title, $content);
$content = preg_replace_callback("/<a.*>.*<\/a>/isU", replace_href, $content);
echo $content;

function replace_title($matches) {
    return preg_replace_callback("/\".*\"/isU",
        function($match) {
            return strtoupper($match[0]);
        },
        $matches[0]
    );
}

function replace_href($matches) {
    return preg_replace_callback("/>[^<]*</",
        function($match) {
            return strtoupper($match[0]);
        },
        $matches[0]
    );
}

?>