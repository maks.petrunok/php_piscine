#!/usr/bin/php
<?php

$days = array("lundi" => 1,
			  "mardi" => 2,
			  "mercredi" => 3,
			  "jeudi" => 4,
			  "vendredi" => 5,
			  "samedi" => 6,
			  "dimanche" => 7);

$months = array("janvier" => 1,
			  "février" => 2,
			  "mars" => 3,
			  "avril" => 4,
			  "mai" => 5,
			  "juin" => 6,
			  "juillet" => 7,
			  "aout" => 8,
			  "septembre" => 9,
			  "octobre" => 10,
			  "novembre" => 11,
			  "décembre" => 12);

$timestamp_exists = false;
$res = 0;
if ($argc == 2)
{
	date_default_timezone_set('Europe/Paris');
	$data = explode(" ", $argv[1]);
	if (count($data) == 5 &&
		preg_match("/^([0-9]|[0-2][0-9]|3[0-1])$/", $data[1]) === 1 &&
		preg_match("/^[0-9]{4}$/", $data[3]) === 1 &&
		preg_match("/^([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/", $data[4]) === 1 &&
		array_key_exists(lcfirst($data[2]), $months)
		)
	{
		$time = explode(":", $data[4]);
		$res = mktime($time[0], $time[1], $time[2], $months[lcfirst($data[2])], $data[1], $data[3]);
		if (date('N', $res) == $days[lcfirst($data[0])])
			$timestamp_exists = true;
	}
}
if ($timestamp_exists)
	echo "$res\n";
else if ($argc > 1)
	echo "Wrong Format\n";

?>
