#!/usr/bin/php
<?php

date_default_timezone_set("Europe/Kiev");

if (!($file = fopen("/var/run/utmpx", 'r'))) {
    echo "Cannot read file\n";
    exit();
}

while ($content = fread($file, 628)) {
    $unpacked = unpack("a256a/a4b/a32c/id/ie/I2f/a256g/i16h", $content);
    $terms[$unpacked['c']] = $unpacked;
}
ksort($terms);
foreach ($terms as $term) {
    if ($term['e'] == 7) {
        echo str_pad(trim($term['a']), 9);
        echo str_pad(trim($term['c']), 9);
        echo date("M d H:i\n", $term['f1']);
    }
}

fclose($file);

?>