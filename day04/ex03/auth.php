<?php

function auth($login, $passwd) {
    $data = "../private/passwd";
    if (file_exists($data)) {
        $users = unserialize(file_get_contents($data));
        foreach($users as $usr) {
            
            if ($usr['login'] == $login) {
                $hash = hash('whirlpool', substr($passwd, 0, 1).substr($usr['login'], 1, 0).$passwd);
                return ($usr['passwd'] == $hash);
            }
        }
    }
    return (false);
}

?>