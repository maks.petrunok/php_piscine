<?php

date_default_timezone_set("Europe/Kiev");
$file = "../private/chat";
$dir = "../private";

session_start();
if ($_SESSION['logged_on_user'] == '')
    exit;

if ($_POST['msg'] != '') {
    if (file_exists($file)) {
        $tmp = fopen($file, 'r');
        flock($tmp, LOCK_SH);
        $content = unserialize(file_get_contents($file));
        flock($tmp, LOCK_UN);
        fclose($tmp);
    } else {
        mkdir($dir);
        $content = array();
    }
    $content[] = array($_SESSION['logged_on_user'], time(), $_POST['msg']);
    $tmp = fopen($file, 'w');
    flock($tmp, LOCK_EX);
    file_put_contents($file, serialize($content));
    flock($tmp, LOCK_UN);
    fclose($tmp);
}

?>

<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script langage="javascript">
        top.frames['chat'].location = 'chat.php';
    </script>
    <style>
        body {
            margin: 0px 0px 0px 0px !important;
        }
        form {
            margin: 0px 0px 0px 0px !important;
        }
        input {
            margin: 0px;
            padding: 0;
            height: 100%;
        }
        input#msg {
            width: 90%;
            border-width: 0;
        }
        input#button {
            width: 8%;
        }
    </style>
</head>
<body>
<form action="./speak.php" method="post">
    <input id="msg" type="text" placeholder="Type your message here..." name="msg" value="" autofocus>
    <input id="button" type="submit" name="submit" value="Send">
</form>
</body>
</html>