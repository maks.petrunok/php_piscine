<?php

$file = "../private/chat";
$dir = "../private";

session_start();
date_default_timezone_set("Europe/Kiev");
if ($_SESSION['logged_on_user'] != '' && file_exists($file)) {
    $tmp = fopen($file, 'r');
    flock($tmp, LOCK_SH);
    $content = unserialize(file_get_contents($file));
    flock($tmp, LOCK_UN);
    fclose($tmp);
} else {
    $content = NULL;
}

?>

<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        body {
            vertical-align: bottom;
            font-family: Arial;
        }
        .meta {
            color: #777777;
        }
        .me {
            background-color: lightblue;
        }
        .other {
            background-color: aquamarine;
        }
    </style>
</head>
<body>
<?php
if ($content != NULL)
{
    foreach($content as $record) {
        if ($record[0] == $_SESSION['logged_on_user'])
            $rec_class = 'me';
        else
            $rec_class = 'other';
        echo '<div class="'.$rec_class.'">';
        echo '<span class="meta">';
        echo "[".$record[0]." at ".date("j M Y G:i:s",$record[1])."] ";
        echo '</span>';
        echo $record[2]."<br/>";
        echo '</div>';
    }
}
else
{
    echo "No messages yet. Be first! :)";
}
echo '<div id="end"></div>';
?>
</body>
</html>