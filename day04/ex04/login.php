<?php

include('auth.php');

session_start();

if ($_POST['login'] != '' && $_POST['passwd'] != '' && auth($_POST['login'], $_POST['passwd']))
{
    $_SESSION['logged_on_user'] = $_POST['login'];

?>

<html>
<head>
    <title>42 Chat</title>
    <style>
        
        iframe#chat {
            vertical-align: text-bottom;
        }
        a {
            text-align: right;
        }
    </style>
</head>
<body>
    <div><a href="./logout.php">Log Out</a></div>
    <br/>
    <iframe name="chat" id="chat" src="chat.php#end" height="550px" width="100%"></iframe>
    <br/>
    <iframe src="speak.php" height="50px" width="100%"></iframe>
</body>
</html>

<?php
}
else
    echo "ERROR\n";

?>