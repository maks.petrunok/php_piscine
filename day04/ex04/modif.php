<?php

function hash_pw($name, $pw) {
    return (hash('whirlpool', substr($pw, 0, 1).substr($name, 1, 0).$pw));
}

$users_data_dir = "../private";
$users_data = $users_data_dir."/passwd";
$login = $_POST['login'];
$old_pw = $_POST['oldpw'];
$new_pw = $_POST['newpw'];

if ($_POST['submit'] != 'OK' || !file_exists($users_data) || $login == '' || $old_pw == '' || $new_pw == '') {
    echo "ERROR\n";
    exit();
}

$users = unserialize(file_get_contents($users_data));
$size = count($users);
for($i = 0; $i < $size; $i++) {
    if ($users[$i]['login'] == $login) {
        $new_hash = hash_pw($login, $new_pw);
        if ($users[$i]['passwd'] == hash_pw($login, $old_pw) && $users[$i]['passwd'] != $new_hash) {
            $users[$i]['passwd'] = $new_hash;
            file_put_contents($users_data, serialize($users));
            echo "OK\n";
            header("Location: index.html");
            exit();
        }
        break;
    }
}

echo "ERROR\n";

?>