<?php

class NightsWatch {

    private static $_fighters = array();

    public function recruit($smb) {
        self::$_fighters[] = $smb;
    }

    public function fight() {
        foreach (self::$_fighters as $fighter) {
            if ($fighter instanceof IFighter) {
                $fighter->fight();
            }
        }
    }
}

?>
