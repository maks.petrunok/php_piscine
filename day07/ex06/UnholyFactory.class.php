<?php

class UnholyFactory {

    public $fTypes = array();

    public function absorb($smb) {
        if ($smb instanceof Fighter) {
            if (array_key_exists($smb->type, $this->fTypes)) {
                echo '(Factory already absorbed a fighter of type ' . $smb->type . ')' . PHP_EOL;
            } else {
                $this->fTypes[$smb->type] = $smb;
                echo '(Factory absorbed a fighter of type ' . $smb->type . ')' . PHP_EOL;
            }
        } else {
            echo '(Factory can\'t absorb this, it\'s not a fighter)' . PHP_EOL;
        }
    }

    public function fabricate($type) {
        if (array_key_exists($type, $this->fTypes)) {
            echo '(Factory fabricates a fighter of type ' . $type . ')' . PHP_EOL;
            return clone $this->fTypes[$type];
        } else {
            echo '(Factory hasn\'t absorbed any fighter of type ' . $type . ')' . PHP_EOL;
            return NULL;
        }
    }
}

?>