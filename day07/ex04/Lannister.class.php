<?php

interface Lannisters {}

class Lannister implements Lannisters {

    public function sleepWith($smb) {
        if ($smb instanceof Lannisters) {
            print('Not even if I\'m drunk !' . PHP_EOL);
        } else {
            print('Let\'s do this.' . PHP_EOL);
        }
    }

}

?>
