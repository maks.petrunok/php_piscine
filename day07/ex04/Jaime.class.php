<?php

class Jaime extends Lannister {

    public function sleepWith($smb) {
        if ($smb instanceof Cersei) {
            print('With pleasure, but only in a tower in Winterfell, then.' . PHP_EOL);
        } else if ($smb instanceof Lannisters) {
            print('Not even if I\'m drunk !' . PHP_EOL);
        } else {
            print('Let\'s do this.' . PHP_EOL);
        }
    }

}

?>