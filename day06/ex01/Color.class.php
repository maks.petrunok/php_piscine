<?php

class Color {
    
    public static $verbose = false;
    
    public $red = 0;
    public $green = 0;
    public $blue = 0;

    static function doc() {
        return file_get_contents('Color.doc.txt') . PHP_EOL;
    }

    function __construct(array $color) {
        if (array_key_exists('rgb', $color)) {
            $this->red   = ($color['rgb'] >> 16 ) & 0xFF;
            $this->green = ($color['rgb'] >> 8 ) & 0xFF;
            $this->blue  = ($color['rgb'] >> 0 ) & 0xFF;
        } else {
            if (array_key_exists('red', $color)) {
                $this->red = max(min(round($color['red']), 255), 0);
            }
            if (array_key_exists('green', $color)) {
                $this->green = max(min(round($color['green']), 255), 0);
            }
            if (array_key_exists('blue', $color)) {
                $this->blue = max(min(round($color['blue']), 255), 0);
            }
        }
        if (self::$verbose)
            echo $this . ' constructed.' . PHP_EOL;
    }

    function __destruct() {
        if (self::$verbose)
            echo $this . ' destructed.' . PHP_EOL;
    }

    function add( Color $color) {
        return new Color(array(
            'red'   => min($this->red   + $color->red, 255),
            'green' => min($this->green + $color->green, 255),
            'blue'  => min($this->blue  + $color->blue, 255)
        ));
    }

    function sub( Color $color) {
        return new Color(array(
            'red'   => max($this->red   - $color->red, 0),
            'green' => max($this->green - $color->green, 0),
            'blue'  => max($this->blue  - $color->blue, 0)
        ));
    }

    function mult($factor) {
        if (is_array($factor)) {
            return new Color(array(
                'red'   => max(min($this->red   * $factor['red'], 255), 0),
                'green' => max(min($this->green * $factor['green'], 255), 0),
                'blue'  => max(min($this->blue  * $factor['blue'], 255), 0)
            ));
        } else {
            return new Color(array(
                'red'   => max(min($this->red   * $factor, 255), 0),
                'green' => max(min($this->green * $factor, 255), 0),
                'blue'  => max(min($this->blue  * $factor, 255), 0)
            ));
        }
    }

    function __toString() {
        return 'Color( red: ' . str_pad($this->red, 3, ' ', STR_PAD_LEFT) .
                ', green: ' . str_pad($this->green, 3, ' ', STR_PAD_LEFT) .
                ', blue: '  . str_pad($this->blue, 3, ' ', STR_PAD_LEFT) .
                ' )';
    }
}

?>