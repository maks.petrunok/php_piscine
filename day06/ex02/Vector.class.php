<?php

require_once ('Color.class.php');
require_once ('Vertex.class.php');

class Vector {

    public static $verbose = false;

    private $_x = 0.0;
    private $_y = 0.0;
    private $_z = 0.0;
    private $_w = 0.0;

    static function doc() {
        return file_get_contents('Vector.doc.txt') . PHP_EOL;
    }

    private static function normalize_point(Vertex &$v) {
        $v->x = $v->x / $v->w;
        $v->y = $v->y / $v->w;
        $v->z = $v->z / $v->w;
        $v->w = 1.0;
    }

    function __construct(array $data) {
        if (!array_key_exists('dest', $data)) {
            die("ERROR: Failed to create Vector object. Destination Vertex is not specified." . PHP_EOL);
        }
        if ($data['dest']->w != 1.0) {
            self::normalize_point($data['dest']);
        }
        if (array_key_exists('orig', $data)) {
            if ($data['orig']->w != 1.0) {
                self::normalize_point($data['orig']);
            }
        } else {
            $data['orig'] = new Vertex(array('x' => 0, 'y' => 0, 'z' => 0));
        }
        $this->_x = $data['dest']->x - $data['orig']->x;
        $this->_y = $data['dest']->y - $data['orig']->y;
        $this->_z = $data['dest']->z - $data['orig']->z;
        if (self::$verbose)
            echo $this . ' constructed' . PHP_EOL;
    }

    function __destruct() {
        if (self::$verbose)
            echo $this . ' destructed' . PHP_EOL;
    }

    function magnitude() {
        return sqrt(pow($this->_x, 2) + pow($this->_y, 2) + pow($this->_z, 2));
    }

    function normalize() {
        $len = $this->magnitude();
        $x = $this->_x / $len;
        $y = $this->_y / $len;
        $z = $this->_z / $len;
        $new = new Vector(array('dest' => new Vertex(array('x' => $x, 'y' => $y, 'z' => $z))));
        return $new;
    }

    function add($rhs) {
        return new Vector(array('dest' => new Vertex(array(
            'x' => $this->_x + $rhs->x, 'y' => $this->_y + $rhs->y, 'z' => $this->_z + $rhs->z
        ))));
    }

    function sub($rhs) {
        return new Vector(array('dest' => new Vertex(array(
            'x' => $this->_x - $rhs->x, 'y' => $this->_y - $rhs->y, 'z' => $this->_z - $rhs->z
        ))));
    }

    function scalarProduct($k) {
        return new Vector(array('dest' => new Vertex(array(
            'x' => $this->_x * $k, 'y' => $this->_y * $k, 'z' => $this->_z * $k
        ))));
    }

    function opposite() {
        return $this->scalarProduct(-1);
    }

    function dotProduct($rhs) {
        return $this->_x * $rhs->x + $this->_y * $rhs->y + $this->_z * $rhs->z;
    }

    function crossProduct($rhs) {
        return new Vector(array('dest' => new Vertex(array(
            'x' => $this->_y * $rhs->z - $this->_z * $rhs->y,
            'y' => $this->_z * $rhs->x - $this->_x * $rhs->z,
            'z' => $this->_x * $rhs->y - $this->_y * $rhs->x,
        ))));
    }

    function cos($rhs) {
        return $this->dotProduct($rhs) / ($this->magnitude() * $rhs->magnitude());
    }

    function __get($att) {
        $att = '_'.$att;
        if (property_exists($this, $att))
            return ($this->$att);
        else if (self::$verbose)
            echo "ERROR: cannot get value: class Vertex does not contain ".substr($att, 1)." property.".PHP_EOL;
    }

    function __set($att, $val) {
        if (self::$verbose)
            echo "ERROR: cannot set value to ".substr($att, 1).". Vector attributes cannot be modified.".PHP_EOL;
    }

    function __toString() {
        $str .= 'Vector( x:' . number_format($this->x, 2) .
                    ', y:' . number_format($this->y, 2) .
                    ', z:' . number_format($this->z, 2) .
                    ', w:' . number_format($this->w, 2) .
                    ' )';
        return $str;
    }

}


?>