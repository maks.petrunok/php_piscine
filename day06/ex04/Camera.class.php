<?php

require_once ('Vertex.class.php');
require_once ('Matrix.class.php');

class Camera {

    public static $verbose = false;

    private $_origin;
    private $_tT;
    private $_tR;
    private $_tRtT;
    private $_tProj;

    static function doc() {
        return file_get_contents('Camera.doc.txt') . PHP_EOL;
    }

    function __construct(array $data) {
        if (
            !array_key_exists('origin', $data) ||
            !array_key_exists('orientation', $data) ||
            !array_key_exists('width', $data) ||
            !array_key_exists('height', $data) ||
            !array_key_exists('fov', $data) ||
            !array_key_exists('near', $data) ||
            !array_key_exists('far', $data)
        )
            die('ERROR: cannot create Camera instance - constructor arguments array not complete.');
        $this->_origin = $data['origin'];
        $this->_tT = new Matrix(array('preset' => Matrix::TRANSLATION,
                'vtc' => (new Vector(array('dest' => $this->_origin)))->opposite()));
        $this->_tR = $data['orientation']->symm();
        $this->_tRtT = $this->_tR->mult($this->_tT);
        $this->_tProj = new Matrix(array('preset' => Matrix::PROJECTION,
                                'fov' => $data['fov'],
                                'ratio' => $data['width'] / $data['height'],
                                'near' => $data['near'],
                                'far' => $data['far'] ));
        if (self::$verbose)
            echo 'Camera instance constructed' . PHP_EOL;
    }

    function __destruct() {
        if (self::$verbose)
            echo 'Camera instance destructed' . PHP_EOL;
    }

    function __toString() {
        $str = 'Camera(' . PHP_EOL .
                '+ Origin: ' . $this->_origin . PHP_EOL .
                '+ tT:' . PHP_EOL . $this->_tT . PHP_EOL .
                '+ tR:' . PHP_EOL . $this->_tR . PHP_EOL .
                '+ tR->mult( tT ):' . PHP_EOL . $this->_tRtT . PHP_EOL .
                '+ Proj:' . PHP_EOL . $this->_tProj . PHP_EOL .
                ')';
        return $str;
    }
}

?>
