<?php

require_once ('Vector.class.php');

class Matrix {

    const IDENTITY = 'identity';
    const SCALE = 'scale';
    const RX = 'rx';
    const RY ='ry';
    const RZ = 'rz';
    const TRANSLATION = 'translation';
    const PROJECTION = 'projection';

    public static $verbose = false;

    private $_x = NULL;
    private $_y = NULL;
    private $_z = NULL;
    private $_o = NULL;

    static function doc() {
        return file_get_contents('Matrix.doc.txt') . PHP_EOL;
    }

    private function createIdentity() {
        $this->_x = array(1.0, 0.0, 0.0, 0.0);
        $this->_y = array(0.0, 1.0, 0.0, 0.0);
        $this->_z = array(0.0, 0.0, 1.0, 0.0);
        $this->_w = array(0.0, 0.0, 0.0, 1.0);
        if (self::$verbose)
            echo 'Matrix IDENTITY instance constructed' . PHP_EOL;
    }

    private function createTranslation(Vector $v) {
        if (!$v)
            die('ERROR: no vector passed to create translation matrix.' . PHP_EOL);
        $this->_x = array(1.0, 0.0, 0.0, $v->x);
        $this->_y = array(0.0, 1.0, 0.0, $v->y);
        $this->_z = array(0.0, 0.0, 1.0, $v->z);
        $this->_w = array(0.0, 0.0, 0.0, 1.0);
        if (self::$verbose)
            echo 'Matrix TRANSLATION preset instance constructed' . PHP_EOL;
    }

    private function createScale(int $k) {
        if (!$k)
            die('ERROR: no scale factor to create scale matrix.' . PHP_EOL);
        $this->_x = array($k,  0.0, 0.0, 0);
        $this->_y = array(0.0, $k,  0.0, 0);
        $this->_z = array(0.0, 0.0, $k,  0);
        $this->_w = array(0.0, 0.0, 0.0, 1.0);
        if (self::$verbose)
            echo 'Matrix SCALE preset instance constructed' . PHP_EOL;
    }

    private function createOX(float $a) {
        if (!$a)
            die('ERROR: no angle specified to create RX matrix.' . PHP_EOL);
        $this->_x = array(1.0,  0.0,    0.0,      0.0);
        $this->_y = array(0.0, cos($a), -sin($a), 0.0);
        $this->_z = array(0.0, sin($a), cos($a),  0.0);
        $this->_w = array(0.0, 0.0,      0.0,     1.0);
        if (self::$verbose)
            echo 'Matrix Ox ROTATION preset instance constructed' . PHP_EOL;
    }

    private function createOY(float $a) {
        if (!$a)
            die('ERROR: no angle specified to create RY matrix.' . PHP_EOL);
        $this->_x = array(cos($a),  0.0, sin($a), 0.0);
        $this->_y = array(0.0,      1.0, 0.0,     0.0);
        $this->_z = array(-sin($a), 0.0, cos($a), 0.0);
        $this->_w = array(0.0,      0.0, 0.0,     1.0);
        if (self::$verbose)
            echo 'Matrix Oy ROTATION preset instance constructed' . PHP_EOL;
    }

    private function createOZ(float $a) {
        if (!$a)
            die('ERROR: no angle specified to create RZ matrix.' . PHP_EOL);
        $this->_x = array(cos($a), -sin($a), 0.0, 0.0);
        $this->_y = array(sin($a), cos($a),  0.0, 0.0);
        $this->_z = array(0.0,     0.0,      1.0, 0.0);
        $this->_w = array(0.0,     0.0,      0.0, 1.0);
        if (self::$verbose)
            echo 'Matrix Oz ROTATION preset instance constructed' . PHP_EOL;
    }

    private function createProj($data) {
        if (!array_key_exists('fov', $data) || !array_key_exists('ratio', $data) ||
            !array_key_exists('near', $data) || !array_key_exists('far', $data))
            die('ERROR: not complete arguments to create PROJECTINO matrix.' . PHP_EOL);
        $aov = $data['fov'];
        $near = $data['near'];
        $far = $data['far'];
        $ratio = $data['ratio'];
        $scale = tan($aov * 0.5 * M_PI / 180) * $near;
        $r = $ratio * $scale;
        $l = -$r;
        $t = $scale;
        $b = -$t;
        $this->_x = array(2 * $near / ($r - $l), 0.0,                   ($r + $l) / ($r - $l),            0.0);
        $this->_y = array(0.0,                   2 * $near / ($t - $b), ($t + $b) / ($t - $b),            0.0);
        $this->_z = array(0.0,                   0.0,                   -($far + $near) / ($far - $near), -2 * $far * $near / ($far - $near));
        $this->_w = array(0.0,                   0.0,                   -1.0,                             0.0);
        if (self::$verbose)
            echo 'Matrix PROJECTION preset instance constructed' . PHP_EOL;
    }

    function __construct(array $data) {
        if ($data['preset'] == self::IDENTITY) {
            $this->createIdentity();
        } else if ($data['preset'] == self::TRANSLATION) {
            $this->createTranslation($data['vtc']);
        } else if ($data['preset'] == self::SCALE) {
            $this->createScale($data['scale']);
        } else if ($data['preset'] == self::RX) {
            $this->createOX($data['angle']);
        } else if ($data['preset'] == self::RY) {
            $this->createOY($data['angle']);
        } else if ($data['preset'] == self::RZ) {
            $this->createOZ($data['angle']);
        } else if ($data['preset'] == self::PROJECTION) {
            $this->createProj($data);
        }
    }

    function __destruct() {
        if (self::$verbose)
            echo 'Matrix instance destructed' . PHP_EOL;
    }

    private function transpose(array $n) {
        for($i = 0; $i < 3; $i++) {
            for($j = $i + 1; $j < 4; $j++) {
                $tmp = $n[$i][$j];
                $n[$i][$j] = $n[$j][$i];
                $n[$j][$i] = $tmp;
            }
        }
        return $n;
    }

    function mult(Matrix $rhs) {
        $verb = self::$verbose;
        self::$verbose = false;
        $m = array($this->_x, $this->_y, $this->_z, $this->_w);
        $n = array($rhs->_x, $rhs->_y, $rhs->_z, $rhs->_w);
        $res = array(array(0,0,0,0), array(0,0,0,0), array(0,0,0,0), array(0,0,0,0));
        for($i = 0; $i < 4; $i++) {
            for($j = 0; $j < 4; $j++) {
                for($k = 0; $k < 4; $k++) {
                    $res[$i][$j] += $m[$i][$k] * $n[$k][$j];
                }
            }
        }
        $new = new Matrix(array('preset' => self::IDENTITY));
        $new->_x = $res[0];
        $new->_y = $res[1];
        $new->_z = $res[2];
        $new->_w = $res[3];
        self::$verbose = $verb;
        return $new;
    }

    function transformVertex(Vertex $vtx) {
        $m = array($this->_x, $this->_y, $this->_z, $this->_w);
        $n = array($vtx->x, $vtx->y, $vtx->z, $vtx->w);
        $res = array(0.0,0.0,0.0,0.0);
        for($i = 0; $i < 4; $i++) {
            for($k = 0; $k < 4; $k++) {
                $res[$i] += ($m[$i][$k] * $n[$k]);   
            }
        }
        $new = new Vertex(array('x' => $res[0], 'y' => $res[1], 'z' => $res[2], 'w' => $res[3]));
        return $new;
    }

    function symm() {
        $m = array($this->_x, $this->_y, $this->_z, $this->_w);
        $res = array(array(0,0,0,0), array(0,0,0,0), array(0,0,0,0), array(0,0,0,0));
        for($i = 0; $i < 4; $i++) {
            for($j = 0; $j < 4; $j++) {
                $res[$i][$j] = $m[$j][$i];
            }
        }
        $new = new Matrix(array('preset' => self::IDENTITY));
        $new->_x = $res[0];
        $new->_y = $res[1];
        $new->_z = $res[2];
        $new->_w = $res[3];
        return $new;
    }

    private function row_fmt(array $row) {
        return number_format($row[0], 2) . ' | ' .
               number_format($row[1], 2) . ' | ' .
               number_format($row[2], 2) . ' | ' .
               number_format($row[3], 2);
    }

    function __toString() {
        $str = 'M | vtcX | vtcY | vtcZ | vtxO' . PHP_EOL .
               '-----------------------------' . PHP_EOL .
               'x | ' . $this->row_fmt($this->_x) . PHP_EOL .
               'y | ' . $this->row_fmt($this->_y) . PHP_EOL .
               'z | ' . $this->row_fmt($this->_z) . PHP_EOL .
               'w | ' . $this->row_fmt($this->_w);
        return $str;
    }
}

?>