
<- Matrix ----------------------------------------------------------------------
This class is a representation of 4x4 matrices.

Constructor supports several matrix types: itentity, scale, RX, RY, RZ,
translation and projection.

Class has the following methods:
mult($matrix) - to multiply two matrices,
transformVertex($vtx) - to transform vertex based on current matrix.
---------------------------------------------------------------------- Matrix ->