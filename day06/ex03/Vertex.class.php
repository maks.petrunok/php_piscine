<?php

require_once('Color.class.php');

class Vertex {

    public static $verbose = false;

    private $_x = 0.0;
    private $_y = 0.0;
    private $_z = 0.0;
    private $_w = 1.0;
    private $_color = NULL;

    static function doc() {
        return file_get_contents('Vertex.doc.txt') . PHP_EOL;
    }

    function __construct(array $data) {
        if (array_key_exists('x', $data) &&
            array_key_exists('y', $data) &&
            array_key_exists('z', $data)) {
                $this->x = $data['x'];
                $this->y = $data['y'];
                $this->z = $data['z'];
        } else {
            die("ERROR: Failed to create Vertex object. Coordinates 'x', 'y' and/or 'z' not specified." . PHP_EOL);
        }
        
        if ($data['w']) {
            $this->w = $data['w'];
        }
        if ($data['color']) {
            $this->color = $data['color'];
        } else {
            $this->color = new Color(array('red' => 255, 'green' => 255, 'blue' => 255));
        }
        if (self::$verbose)
            echo $this . ' constructed' . PHP_EOL;
    }

    function __destruct() {
        if (self::$verbose)
            echo $this . ' destructed' . PHP_EOL;
    }

    function __get($att) {
        $att = '_'.$att;
        if (property_exists($this, $att))
            return ($this->$att);
        else if (self::$verbose)
            echo "ERROR: cannot get value: class Vertex does not contain ".substr($att, 1)." property.".PHP_EOL;
    }

    function __set($att, $val) {
        $att = '_'.$att;
        if (property_exists($this, $att))
            $this->$att = $val;
        else if (self::$verbose)
            echo "ERROR: cannot set value: class Vertex does not contain ".substr($att, 1)." property.".PHP_EOL;
    }

    function __toString() {
        $str .= 'Vertex( x: ' . number_format($this->x, 2) .
                    ', y: ' . number_format($this->y, 2) .
                    ', z: ' . number_format($this->z, 2) .
                    ', w: ' . number_format($this->w, 2);

        if (self::$verbose)
            $str .= ', ' . $this->color;
        $str .= ' )';
        return $str;
    }
}

?>