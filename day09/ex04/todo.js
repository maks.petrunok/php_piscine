$(document).ready(readCookie);
$(document).ready(function() {
    $("#new").click(createTask);
});

function createTask() {
    var task = prompt('Add new task:', 'Feed the cat.').trim();
    if (task) {
        var newTask = document.createElement('div');
        newTask.innerHTML = task;
        newTask.addEventListener("click", deleteTask);
        document.getElementById('ft_list').prepend(newTask);
        $.post("insert.php",
            {
                add: task
            },
        );
    }
}

function deleteTask() {
    if (confirm("Are you sure to remove the task:\n" + this.innerHTML + "?")) {
        $.post("delete.php",
        {
            del: this.innerHTML
        });
        this.remove();
    }
}

function updateCookie(name) {
    var expire = new Date();
    expire.setDate(expire.getDate() + 3);
    document.cookie = "list=" + document.getElementById('ft_list').innerHTML + "; expires=" + expire.toUTCString();
}

function readCookie() {
    $.post("select.php",
    {
        get_list: "get"
    },
    function(data) {
        if (data) {
            var resp = JSON.parse(data);
            if (resp) {
                for (var i = 0; i < resp.length; i++) {
                    if (resp[i] != '') {
                        var newTask = document.createElement('div');
                        newTask.innerHTML = resp[i];
                        newTask.addEventListener("click", deleteTask);
                        document.getElementById('ft_list').prepend(newTask);
                    }
                }
            }
        }
    });
}