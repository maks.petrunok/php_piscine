<?php

$file = 'list.csv';

if ($_POST['del']) {
    if (file_exists($file)) {
        $list = unserialize(file_get_contents($file));
        for ($i = 0; $i < count($list); $i++) {
            if ($list[$i] == $_POST['del']) {
                $list[$i] = '';
                break;
            }
        }
        file_put_contents($file, serialize($list));
    }
}

?>