function createTask() {
    var task = prompt('Add new task:', 'Feed the cat.').trim();
    if (task) {
        var newTask = document.createElement('div');
        newTask.innerHTML = task;
        newTask.addEventListener("click", deleteTask);
        document.getElementById('ft_list').prepend(newTask);
        updateCookie();
    }
}

function deleteTask() {
    if (confirm("Are you sure to remove the task:\n" + this.innerHTML + "?")) {
        this.remove();
        updateCookie();
    }
}

function updateCookie(name) {
    var expire = new Date();
    expire.setDate(expire.getDate() + 3);
    document.cookie = "list=" + document.getElementById('ft_list').innerHTML + "; expires=" + expire.toUTCString();
}

function readCookie() {
    var cooks = document.cookie.replace('list=', '');
    document.getElementById('ft_list').innerHTML = cooks;
    var items = document.getElementById('ft_list').childNodes;
    for (var i=0; i < items.length; i++) {
        items[i].addEventListener("click", deleteTask);
    }
}
