INSERT INTO db_mpetruno.ft_table
(login, creation_date, `group`)
SELECT last_name, birthdate, 'other' as group_val
FROM db_mpetruno.user_card
WHERE last_name LIKE '%a%' AND LENGTH(last_name) < 9
ORDER BY last_name ASC
LIMIT 10;
